# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Germany',
    'name_de_DE': 'Buchhaltung Deutschland',
    'version': '2.2.0',
    'author': 'MBSolutions',
    'email': 'info@m9s.biz',
    'website': 'http://www.m9s.biz',
    'description': '''Add-Ons for Germany
    - Provides accounting features typically required in Germany

      * Sets dependencies to required modules

      Contains:

     * Reports ?

''',
    'description_de_DE': '''Erweiterungen für Deutschland
    - Stellt Erweiterungen für die Buchhaltung zur Verfügung, die
      typischerweise in Deutschland benötigt werden.

      * Setzt Abhängigkeiten für benötigte Module

      Enthält:

      *

''',
    'depends': [
        'ir',
        'res',
        #'account',
        #'account_batch',
        #'account_batch_timeline',
        #'account_batch_tax',
        #'account_batch_tax_timeline',
        #'account_export',
        #'account_move_external_reference',
        #'account_move_forbid_update_posted',
        ##'account_move_reverse', # not compatible with account batch
                                  # s. #678
        #'account_option_party_required',
        #'account_period_close_strict',
        #'account_product',
        #'account_product_rule',
        #'account_tax_code_type',
        #'account_timeline',
        #'account_timeline_datev',
        #'account_timeline_product_rule',
        #'account_timeline_tax_de',
        #'account_timeline_tax_de_datev',
        #'party',
        #'party_addressee',
    ],
    'xml': [
    ],
    'translation': [
        # 'locale/de_DE.po',
    ],
}

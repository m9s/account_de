# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.tests.test_tryton import ModuleTestCase


class AccountDeTestCase(ModuleTestCase):
    "Test Account De module"
    module = 'account_de'


del ModuleTestCase
